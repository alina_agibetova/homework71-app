import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipes } from '../recipes.model';
import { Subscription } from 'rxjs';
import { RecipesService } from '../recipes.service';
import { ActivatedRoute, Data, Router } from '@angular/router';

@Component({
  selector: 'app-all-info',
  templateUrl: './all-info.component.html',
  styleUrls: ['./all-info.component.css']
})
export class AllInfoComponent implements OnInit, OnDestroy {
  recipe!: Recipes;


  constructor(private recipesService: RecipesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.recipe = <Recipes>data.recipes;
    });

  }

  ngOnDestroy(): void {

  }

}
