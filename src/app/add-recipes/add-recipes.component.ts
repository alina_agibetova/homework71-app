import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipes, Step } from '../recipes.model';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipesService } from '../recipes.service';

@Component({
  selector: 'app-add-recipes',
  templateUrl: './add-recipes.component.html',
  styleUrls: ['./add-recipes.component.css']
})
export class AddRecipesComponent implements OnInit, OnDestroy {
  isUpLoading = false;
  recipesUpLoadingSubscription!: Subscription;
  editId = '';
  recipes!: Recipes;
  recipe!: Recipes;
  isEdit = false;
  profileForm!: FormGroup;
  isClick = true;

  constructor(private router: Router, private recipesService: RecipesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.profileForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      imageUrl: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    });

    this.recipesUpLoadingSubscription = this.recipesService.recipesUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });

    this.route.data.subscribe(data => {
      const recipe = <Recipes | null> data.recipes;
      this.recipes = data.recipes;
      if (recipe){
        this.isEdit = true;
        this.editId = recipe.id;
        this.setFormValue({
          name: recipe.name,
          description: recipe.description,
          imageUrl: recipe.imageUrl,
          ingredients: recipe.ingredients,
          steps: [],
        })
      } else {
        this.isEdit = false;
        this.editId = '';
        this.setFormValue({
          name: '',
          description: '',
          imageUrl: '',
          ingredients: '',
          steps: [],
        })
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.profileForm.setValue(value);
    });
  }

  fieldHasError(fieldName: string, errorType: string){
    const field = this.profileForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addStep(){
    if (this.isEdit){
      this.isClick = false;
      this.recipes.steps.forEach( (item: Step) => {
        const steps = <FormArray>this.profileForm.get('steps');
        const stepGroup = new FormGroup({
          imageStep: new FormControl(`${item.imageStep}`, Validators.required),
          descriptionStep: new FormControl(`${item.descriptionStep}`, Validators.required)
        });
        steps.push(stepGroup);
      })

    } else {
      this.isClick = false;
      const steps = <FormArray>this.profileForm.get('steps');
      const stepGroup = new FormGroup({
        imageStep: new FormControl(``, Validators.required),
        descriptionStep: new FormControl(``, Validators.required)
      });
      steps.push(stepGroup);
    }
  }

  getStepControls(){
    const steps = <FormArray>this.profileForm.get('steps');
    return steps.controls;
  }

  onSubmit(){
    const id = this.editId || Math.random().toString();
    const formRecipe = new Recipes(id, this.profileForm.value.name,
      this.profileForm.value.description, this.profileForm.value.imageUrl,
      this.profileForm.value.ingredients, this.profileForm.value.steps);
    const next = () => {
      this.recipesService.fetchRecipes();
      void this.router.navigate(['/'], {relativeTo: this.route});
    };

    if (this.isEdit) {
      this.recipesService.editRecipes(formRecipe).subscribe(next);
    } else {
      this.recipesService.getFormRecipes(formRecipe).subscribe(next);
    }
  }

  ngOnDestroy(): void {
    this.recipesUpLoadingSubscription.unsubscribe();
  }

}
