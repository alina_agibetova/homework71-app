import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipes } from './recipes.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class RecipesService {
  recipesUpLoading = new Subject<boolean>();
  recipesChange = new Subject<Recipes[]>();
  recipesFetching = new Subject<boolean>();
  recipesRemoving = new Subject<boolean>();
  private recipes: Recipes[] = [];

  constructor(private http: HttpClient){}

  fetchRecipes(){
    this.recipesFetching.next(true);
    this.http.get<{[id: string]: Recipes}>('https://alina-beaf9-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const recipeData = result[id];
          return new Recipes(
            id,
            recipeData.name,
            recipeData.description,
            recipeData.imageUrl,
            recipeData.ingredients,
            recipeData.steps
          );
        })
      })).subscribe(recipes => {
        this.recipes = recipes;
        this.recipesChange.next(this.recipes.slice());
        this.recipesFetching.next(false);
    }, error => {
        this.recipesFetching.next(false);
    })
  }

  getFormRecipes(recipes: Recipes){
    const body = {
      name: recipes.name,
      description: recipes.description,
      imageUrl: recipes.imageUrl,
      ingredients: recipes.ingredients,
      steps: recipes.steps
    };

    this.recipesUpLoading.next(true);
    return this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/recipes.json', body).pipe(
      tap(() => {
        this.recipesUpLoading.next(false);
      }, () => {
        this.recipesUpLoading.next(false);
      })
    )
  }

  getRecipes(){
    return this.recipes.slice();
  }

  fetchRecipe(id: string){
    return this.http.get<Recipes | null>(`https://alina-beaf9-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      map(result => {
        if (!result){
          return null;
        }
        return new Recipes(
          id, result.name,
          result.description,
          result.imageUrl,
          result.ingredients,
          result.steps)
      })
    );
  }

  editRecipes(recipes: Recipes){
    this.recipesUpLoading.next(true);
    const body = {
      name: recipes.name,
      description: recipes.description,
      imageUrl: recipes.imageUrl,
      ingredients: recipes.ingredients,
      steps: recipes.steps
    };

    return this.http.put(`https://alina-beaf9-default-rtdb.firebaseio.com/register/${recipes.id}.json`, body).pipe(
      tap(() => {
        this.recipesUpLoading.next(false);
      }, () => {
        this.recipesUpLoading.next(false);
      })
    );
  }

  removeRecipe(id: string){
    this.recipesRemoving.next(true);
    return this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      tap(() => {
        this.recipesRemoving.next(false);
      }, () => {
        this.recipesRemoving.next(false);
      })
    )
  }
}
