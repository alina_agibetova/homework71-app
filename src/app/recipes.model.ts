export class Recipes {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public imageUrl: string,
    public ingredients: string,
    public steps: Step[],
  ){}
}

export class Step {
  constructor(
    public imageStep: string,
    public descriptionStep: string
  ){}
}
