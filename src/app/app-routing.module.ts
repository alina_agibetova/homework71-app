import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes/recipes.component';
import { AddRecipesComponent } from './add-recipes/add-recipes.component';
import { RecipesResolverService } from './recipes-resolver.service';
import { AllInfoComponent } from './all-info/all-info.component';

const routes: Routes = [
  {path: '', component: RecipesComponent},
  {path: 'add', component: AddRecipesComponent},
  {path: ':id/edit', component: AddRecipesComponent, resolve: {recipes: RecipesResolverService}},
  {path: ':id/all-info', component: AllInfoComponent, resolve: {recipes: RecipesResolverService}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
