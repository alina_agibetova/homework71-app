import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipes } from '../recipes.model';
import { Subscription } from 'rxjs';
import { RecipesService } from '../recipes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit, OnDestroy {
  recipes: Recipes[] = [];
  isRemoving = false;
  isFetching = false;
  recipeChangeSubscription!: Subscription;
  recipeFetchingSubscription!: Subscription;
  recipeRemovingSubscription!: Subscription;

  constructor(private recipesService: RecipesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.recipes = this.recipesService.getRecipes();
    this.recipeChangeSubscription = this.recipesService.recipesChange.subscribe((recipes: Recipes[]) => {
      this.recipes = recipes;
    });

    this.recipeFetchingSubscription = this.recipesService.recipesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.recipeRemovingSubscription = this.recipesService.recipesRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });

    this.recipesService.fetchRecipes();
  }

  onDelete(id: string){
    this.recipesService.removeRecipe(id).subscribe(() => {
      this.recipesService.fetchRecipes();
      void this.router.navigate(['/'], {relativeTo: this.route});
    });
  }

  ngOnDestroy(): void {
    this.recipeChangeSubscription.unsubscribe();
    this.recipeFetchingSubscription.unsubscribe();
    this.recipeRemovingSubscription.unsubscribe();
  }

}
