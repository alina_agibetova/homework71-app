import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Recipes } from './recipes.model';
import { RecipesService } from './recipes.service';


@Injectable({
  providedIn: 'root'
})
export class RecipesResolverService implements Resolve<Recipes>{

  constructor(private recipesService: RecipesService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipes> | Observable<never> {
    const id = <string>route.params['id'];
    return this.recipesService.fetchRecipe(id).pipe(mergeMap(recipe => {
      if (recipe){
        return of (recipe);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
